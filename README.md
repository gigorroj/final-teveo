# Final-TeVeO

Repositorio de plantilla para el proyecto final del curso 2023-2024.

# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre:Javier Gigorro de las Peñas
* Titulación:Doble Grado en Ingenieria en Sistemas de Telecomunicacion y ADE
* Cuenta en laboratorios: gigorroj
* Cuenta URJC:j.gigorro.2016@alumnos.urjc.es
* Video básico (url):https://youtu.be/-Rgi5BDhJic
* Video parte opcional (url):https://youtu.be/Ce_mPoO7mzE
* Despliegue (url):http://gigorroj.pythonanywhere.com/TeVeO/
* Contraseñas: No necesita autenticacion
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
La aplicacion web TeVeO muestra en su pantalla principal un listado con los comentarios de ciertas camaras ordenados de mas reciente a mas antiguo. Cada comentario tiene el identificador de la camara a la que se refiere el cual es un enlace a la pagina de esa camara.
La pagina de camaras muestra un listado de las fuentes disponibles para descargar informacion de las camaras si estas no estan ya descargadas, tambien aparece una imagen aleatoria de una de las camaras y debajo un listado con las camaras disponibles junto dos enlaces uno a la pagina de la camara y otro a la pagina dinamica de la camara.
La pagina de la camara muestra la imagen actual de la camara con su informacion(lugar, coordenadas) y un listado con los comentarios de esta camara de mas nuevos a mas viejos. Tambien aparece el enlace a la pagina dinamica y a la pagina comentario para poder escribir un comentario de la camara.
La pagina comentario muestra la informacion de la camara, la imagen actual, fecha y un formulario para poner un comentario.
La pagina dinamica de la camara es igual que la pagina de camara asolo que solicita cada 30 segundos la imagen y los comentarios.
Tenemos una pagina de configuracion que permite elegir el nombre del comentador tipo y tamaño de letra.
Y por ultimo una pagina de ayuda donde viene la autoria de la practica y una descripcion de la misma.
## Lista partes opcionales

* Nombre parte: Inclusion del favicon. Se añade el icono favicon.ico en la aplicacion web.
* Nombre parte: Cerrar sesion. En la cabecera de todos los recursos aparece debajo del nombre del comentador un link que permite cerrar sesion y al pulsar sobre el hace que se cierre la sesion actual y te devuelve a la pagina principal como si fuera la primera vez que la visitas.
