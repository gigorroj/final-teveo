from django.contrib import admin
from .models import Contenido, Comentario, Configuracion

# Register your models here.
admin.site.register(Contenido)
admin.site.register(Comentario)
admin.site.register(Configuracion)