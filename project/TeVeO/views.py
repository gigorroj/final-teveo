import requests
import random
import os
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.template import loader
from .models import Contenido, Comentario, Configuracion
from django.utils import timezone
from .forms import ComentarioForm, ConfiguracionForm
import xml.etree.ElementTree as ET
from django.conf import settings
from django.db.models import Count
from PIL import Image
from io import BytesIO
from django.contrib.auth import logout
from django.contrib import messages


# Create your views here.

def procesar_xml(xml_file):
    path_to_xml = os.path.join(settings.BASE_DIR, xml_file)
    tree = ET.parse(path_to_xml)
    root = tree.getroot()

    camaras = []

    for camara in root.findall('camara'):
        camara_info = {}
        camara_id = camara.find('id').text
        camara_info['id'] = f"LIS1-{camara_id}"
        camara_info['src'] = camara.find('src').text
        camara_info['lugar'] = camara.find('lugar').text
        camara_info['coordenadas'] = camara.find('coordenadas').text
        camaras.append(camara_info)

        Contenido.objects.update_or_create(
            clave=camara_info['id'],
            valor=camara_info['src'],
            lugar=camara_info['lugar'],
            coordenadas=camara_info['coordenadas'],
        )
    for camara in root.findall('cam'):
        camara_info = {}
        camara_id = camara.get('id')
        camara_info['id'] = f"LIS2-{camara_id}"
        camara_info['src'] = camara.find('url').text
        camara_info['lugar'] = camara.find('info').text
        latitude = camara.find('place').find('latitude').text
        longitude = camara.find('place').find('longitude').text
        camara_info['coordenadas'] = f"{latitude}, {longitude}"
        camaras.append(camara_info)

        Contenido.objects.update_or_create(
            clave=camara_info['id'],
            valor=camara_info['src'],
            lugar=camara_info['lugar'],
            coordenadas=camara_info['coordenadas'],
        )

    return camaras

def camaras(request):
    camaras_info = []
    if request.method == "POST":
        if 'listado1.xml' in request.POST:
            camaras_info = procesar_xml('listado1.xml')
        elif 'listado2.xml' in request.POST:
            camaras_info = procesar_xml('listado2.xml')
    todas_camaras = Contenido.objects.annotate(num_comentarios=Count('comentario')).order_by('-num_comentarios')
    camara_aleatoria = random.choice(todas_camaras) if todas_camaras else None
    return render(request, 'TeVeO/camaras.html', {'camaras_info': todas_camaras, 'camara_aleatoria': camara_aleatoria})


def capturar_imagen(camara_url):
    response = requests.get(camara_url)
    if response.status_code == 200:
        img = Image.open(BytesIO(response.content))
        filename = f'{random.randint(10000, 99999)}.jpg'
        fileroute = os.path.join(settings.MEDIA_ROOT, filename)
        img.save(fileroute)
        return f'media/{filename}'
    return None



def index(request):
    comment_list = Comentario.objects.all().order_by('-fecha')
    template = loader.get_template('TeVeO/index.html')
    context = {
        'comment_list': comment_list
    }
    return HttpResponse(template.render(context, request))


def get_content(request, clave):
    try:
        contenido = Contenido.objects.get(clave=clave)
        comment_list = contenido.comentario_set.all().order_by('-fecha')
    except Contenido.DoesNotExist:
        return redirect('principal')
    return render(request, 'TeVeO/content.html', {'comment_list': comment_list, 'content': contenido})

def comment_new(request, clave):
    if request.method == "POST":
        form = ComentarioForm(request.POST)
        if form.is_valid():
            contenido = Contenido.objects.get(clave=clave)
            imagen = capturar_imagen(contenido.valor)
            if imagen :
                fecha = timezone.now()
                comentario = Comentario(contenido=contenido, titulo=request.POST['titulo'], imagen=imagen,  cuerpo=request.POST['cuerpo'], fecha=fecha)
                comentario.save()
                return redirect('get_content', clave=contenido.clave)
            else:
                messages.error(request,'No se pudo capturar la imagen')
    contenido = Contenido.objects.get(clave=clave)
    imagen = contenido.valor
    fecha = timezone.now()
    form = ComentarioForm()
    return render(request, 'TeVeO/comment_edit.html', {'content': contenido, 'form': form, 'imagen': imagen, 'fecha_actual': fecha})

def configuracion(request):
    if not request.session.session_key :
        request.session.save()
    session_id = request.session.session_key
    try:
        configuracion_usuario = Configuracion.objects.get(session_key=session_id)
    except Configuracion.DoesNotExist:
        configuracion_usuario = Configuracion(
            session_key=session_id,
            nombre_comentador='Anonimo',
            tamano_letra=12,
            tipo_letra='Arial'
        )
    if request.method == "POST":
        form = ConfiguracionForm(request.POST, instance=configuracion_usuario)
        if form.is_valid():
            form.save()
            return redirect('configuracion')
    else:
        form = ConfiguracionForm(instance=configuracion_usuario)
    return render(request, 'TeVeO/configuracion.html', {'form': form, 'session_id': session_id})

def ayuda(request):
    return render(request, 'TeVeO/ayuda.html',{})

def obtener_datos_camaras(request, clave):
    content = Contenido.objects.get(clave=clave)
    comments = content.comentario.all()
    return render(request,'camaras_partial.html',{'content': content, 'comment_list': comments})

def get_content_dyn(request, clave):

    contenido = Contenido.objects.get(clave=clave)
    comment_list = contenido.comentario_set.all().order_by('-fecha')
    return render(request, 'TeVeO/content_dyn.html', {'comment_list': comment_list, 'content': contenido})

def obtener_datos_camaras_json(request, clave):
    camara = Contenido.objects.get(clave=clave)
    comment_list = camara.comentario_set.all().order_by('-fecha')
    datos_camara = {
        'id': camara.clave,
        'valor': camara.valor,
        'lugar': camara.lugar,
        'coordenadas': camara.coordenadas,
        'comentarios': [
            {
                'titulo': comentario.titulo,
                'cuerpo': comentario.cuerpo,
                'fecha': comentario.fecha,
                'imagen': comentario.imagen
            }
            for comentario in comment_list],
        'numero_comentarios': comment_list.count()
    }
    return JsonResponse(datos_camara)

def logout_view(request):
    current_session_id =request.session.session_key
    Configuracion.objects.filter(session_key=current_session_id).delete()
    logout(request)
    return redirect('principal')
