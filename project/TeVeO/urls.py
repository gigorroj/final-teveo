from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib.auth.views import LogoutView
from . import views

urlpatterns = [
    path("", views.index, name='principal'),
    path("camaras", views.camaras, name='camaras'),
    path("comentario/<str:clave>", views.comment_new, name='comentario'),
    path("configuracion", views.configuracion, name='configuracion'),
    path("ayuda", views.ayuda, name='ayuda'),
    path("obtener_datos_camara_json/<str:clave>", views.obtener_datos_camaras_json),
    path("logout", views.logout_view),
    path("<str:clave>-dyn.", views.get_content_dyn, name='get_content_dyn'),
    path("<str:clave>", views.get_content, name='get_content'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)