from django import forms
from .models import Comentario, Configuracion

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ('titulo', 'cuerpo')

class ConfiguracionForm(forms.ModelForm):
    class Meta:
        model = Configuracion
        fields = ('nombre_comentador', 'tamano_letra', 'tipo_letra')