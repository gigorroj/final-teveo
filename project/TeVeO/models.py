from django.db import models

# Create your models here.
class Contenido(models.Model):
    clave = models.CharField(max_length=256)
    valor = models.CharField(max_length=256)
    lugar = models.CharField(max_length=256)
    coordenadas = models.CharField(max_length=256)
    def __str__(self):
        return self.clave

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=256)
    imagen = models.TextField()
    cuerpo = models.TextField(blank=False)
    fecha = models.DateTimeField('published')

    def __str__(self):
        return self.titulo

class Configuracion(models.Model):
    session_key = models.CharField(max_length=50)
    nombre_comentador = models.CharField(max_length=100, default='Anonimo')
    tamano_letra = models.IntegerField(default=12)
    tipo_letra = models.CharField(max_length=50, default='Arial')

    def __str__(self):
        return self.nombre_comentador
