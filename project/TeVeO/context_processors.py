from .models import Contenido, Comentario, Configuracion
from django.contrib.sessions.models import Session

def configuracion_global(request):
    current_session_id = request.session.session_key
    try:
        configuracion_usuario = Configuracion.objects.get(session_key=current_session_id)
    except Configuracion.DoesNotExist:
        configuracion_usuario = Configuracion(
            session_key= current_session_id,
            nombre_comentador= 'Anonimo',
            tamano_letra=12,
            tipo_letra='Arial'
        )
    return {'configuracion_usuario': configuracion_usuario}


def total(request):
    camaras = Contenido.objects.count()
    comentarios = Comentario.objects.count()
    return {
        'total_camaras': camaras,
        'total_comentarios': comentarios
    }