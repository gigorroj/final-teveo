from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from .models import Configuracion


class HomeTest(TestCase):
    def test_pagina_principal(self):
        response = self.client.get(reverse('principal'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')


class ConfigTest(TestCase):
    def test_pagina_configuracion(self):
        session_key = 'example_session_key'
        response = self.client.get(reverse('configuracion'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'configuracion.html')
        form_data = {
            'session_key': session_key,
            'nombre_comentador': 'Gines',
            'tamano_letra': '20',
            'tipo_letra': 'Times New Romans',
        }
        response = self.client.post(reverse('configuracion'), data=form_data)
        self.assertEqual(response.status_code, 200)
        updated_session = Configuracion.objects.get(session_key=session_key)
        self.assertEqual(updated_session.nombre_comentador, 'Gines')
        self.assertEqual(updated_session.tamano_letra, '20')
        self.assertEqual(updated_session.tipo_letra, 'Times New Romans')

class AyudaTest(TestCase):

    def test_pagina_ayuda(self):

        response = self.client.get(reverse('ayuda'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'ayuda.html')

class CamarasTest(TestCase):
    def test_camaras(self):
        response = self.client.get(reverse('camaras'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camaras.html')
class CamaraTest(TestCase):

    def test_camara(self):
        response = self.client.get('/LIS1-4')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'content.html')

    def test_camara_dinamica(self):
        response = self.client.get('/LIS1-4-dyn.')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'content-dyn.html')

    def test_camara_json(self):
        response = self.client.get('obtener_datos_camara_json/LIS1-4')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'obtener_datos_camara_json.html')
class ComentarioTest(TestCase):
    def test_comentario(self):
        response = self.client.get('/comentario/LIS2-A')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'comment_edit.html')
        form_data = {
            'contenido': 'LIS2-A',
            'titulo': 'Un comentario',
            'imagen': '11155.jpg',
            'cuerpo': 'Probando a poner un comentario',
            'fecha' : timezone.now()
        }
        response = self.client.post('/comentario/LIS2-A', data=form_data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'comment_edit.html')

